package com.chmelar.doresiosk;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.loopj.android.http.PersistentCookieStore;


import java.net.CookieManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.editTextMail)
    EditText mail;
    @Bind(R.id.editTextPassword)
    EditText password;


    public static PersistentCookieStore myCookieStore;

    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ok = false;
        ButterKnife.bind(this);
        prefs = this.getSharedPreferences(
                "com.chmelar.doresiosk", Context.MODE_PRIVATE);

        try {
            if (!(getIntent().getExtras().get("flag") == "no")) {
                mail.setText("mail");
                password.setText("password");

            }
        } catch (NullPointerException e) {
            try {
                mail.setText(prefs.getString("mail", "mail"));
                password.setText(prefs.getString("password", "password"));
            } catch (Exception ex) {
            }
        }
        onClickLogin();
    }

    private boolean ok = false;

    @OnClick(R.id.buttonLogin)
    void onClickLogin() {

        final String txtMail = mail.getText().toString().trim();
        final String txtPass = password.getText().toString().trim();
        prefs.edit().putString("mail", txtMail).apply();
        prefs.edit().putString("password", txtPass).apply();


        final Context context = this;

        try {

            Ion.with(context)
                    .load("POST", "http://doresio.sk/?l=1&s=1")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .setBodyParameter("email", txtMail)
                    .setBodyParameter("password", txtPass)
                    .setBodyParameter("submit_login", "Prihlásiť")
                    .asString()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<String>>() {
                        @Override
                        public void onCompleted(Exception e, Response<String> result) {
                            try {
                                if (result.getResult().contains("Nesprávny")) {
                                    Toast.makeText(LoginActivity.this, "Nesprávny e-mail alebo heslo.", Toast.LENGTH_SHORT).show();
                                    ok = false;
                                } else {
                                    ok = true;
                                }
                            } catch (Exception e1) {
                                Toast.makeText(LoginActivity.this, "Problem s pripojenim", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).get();
            if (ok) {
                Intent intent = new Intent(context, MessagesActivity.class);
                startActivity(intent);
                TimeService.isLogged = true;
                TimeService.isLogged = true;
                ok = false;
                Ion.getDefault(this).cancelAll();
                finish();

            }
        } catch (Exception e) {
            Toast.makeText(LoginActivity.this, "Problem s pripojenim", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.editTextMail)
    void clearMail() {
        mail.setText("");
    }

    @OnClick(R.id.editTextPassword)
    void clearPassword() {
        password.setText("");
    }
}
