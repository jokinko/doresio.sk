package com.chmelar.doresiosk;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

/**
 * Created by jozef.chmelar.ml on 21. 4. 2016.
 */

public class TimeService extends Service {
    // constant
    public static final long NOTIFY_INTERVAL = 120 * 1000; // x* 1second
    //    private static final int NOTIFY_DELAY = 0 * 1000;
    private SharedPreferences prefs;
    private ArrayList<Message> listMessages = new ArrayList<>();
    private ArrayList<Message> listMessagesComparator = new ArrayList<>();
    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;
    private Timer mTimerList = null;
    private String meno;
    private String heslo;
    private boolean added = false;
    public static boolean isLogged;
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        isLogged = false;
        //bude bezat aj ked sa zamkne
        powerManager = (PowerManager) getApplicationContext().getSystemService(getApplicationContext().POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DORESIO");
        wakeLock.acquire();
        // cancel if already existed
        if (mTimer != null) {
            mTimer.cancel();
        } else {
            mTimer = new Timer();
            mTimerList = new Timer();
        }
        // schedule task

        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);
        mTimerList.scheduleAtFixedRate(new SendListTask(), 0, 500);

    }

    @Override
    public void onDestroy() {
        Ion.with(this).load("GET", "http://doresio.sk/?l=1&s=1&submit_logout=1"); //logout na stranke
        isLogged = false;
        wakeLock.release();
        Ion.getDefault(this).cancelAll();
        Ion ion = Ion.getDefault(this);
        ion.getCookieMiddleware().getCookieStore().removeAll();
        Toast.makeText(TimeService.this, "logout z doresio.sk", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    //region Slucka
    class SendListTask extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    sendList();
                }
            });
        }
    }

    class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    sendMessage();
                }
            });
        }
    }
    //endregion

    private void sendList() {
        Intent intent = new Intent("my-event");
        ArrayList<Message> list = listMessages;
        intent.putExtra("list", list);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendMessage() {
        Intent intent = new Intent("my-event");
        // add data
        intent.putExtra("message", "data is loggeed " + isLogged);
        if (!isLogged)
            login();
        getmsg();
        ArrayList<Message> list = listMessages;
        intent.putExtra("list", list);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void getmsg() {
        Ion.with(this).load("GET", "http://doresio.sk/?l=1&s=22")
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        try {
                            Document doc = Jsoup.parse(result.getResult());
                            Elements elementsPerson = doc.getElementsByTag("strong"); //dostanem osoby
                            boolean newMessageInList = false;

                            for (Element elementPerson : elementsPerson) {
                                String messageAuthor = elementPerson.text();
                                String messageCount = elementPerson.parent().getElementsByTag("span").text();

                                boolean isNewMessage = false;
                                if (!(messageCount.isEmpty() || messageCount == null))
                                    isNewMessage = Integer.parseInt(messageCount) > 0;

                                newMessageInList = newMessageInList || isNewMessage;
                                Message message = new Message(messageCount, messageAuthor);

                                if (isNewMessage) {
                                    Boolean newMessageFromTheSameAuthor = false;

                                    for (Message m : listMessages) {
                                        if (m.isSameAuthor(message)) {
                                            newMessageFromTheSameAuthor = true;
                                        }
                                    }
                                    if (newMessageFromTheSameAuthor) {
                                        int index = listMessages.indexOf(message);
                                        //get the message from the same author from list.
                                        Message m = listMessages.get(index);
                                        //if count is different change it and mark as added
                                        if (m.getCount() != message.getCount()) {
                                            m.setCount(message.getCount());
                                            listMessages.set(index, m);
                                            added = true;
                                        }
                                    } else {
                                        listMessages.add(message);
                                        added = true;
                                    }

                                } else {
                                    newMessageInList = newMessageInList || isNewMessage;
                                }
                            }
                            if (added) {
                                added = false;
                                if (!listMessages.equals(listMessagesComparator)) {
                                    listMessagesComparator = listMessages;
                                    notification(listMessages.get(listMessages.size() - 1).toString());
                                }
                            }
                            if (!newMessageInList) listMessages = new ArrayList<Message>();
                        } catch (Exception e1) {
                            listMessages = new ArrayList<Message>();
                        }
                    }
                });
    }

    private void login() {

        try {
            try {
            //    meno = (prefs.getString("mail", "mail"));
              //  heslo = (prefs.getString("password", "password"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            Ion.with(this)
                    .load("POST", "http://doresio.sk/?l=1&s=1")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .setBodyParameter("email", meno)
                    .setBodyParameter("password", heslo)
                    .setBodyParameter("submit_login", "Prihlásiť")
                    .asString()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<String>>() {
                        @Override
                        public void onCompleted(Exception e, Response<String> result) {
                            try {
                                if (result.getResult().contains("Nesprávny e-mail alebo heslo.")) {
                                } else {
                                    TimeService.isLogged = true;
                                }
                            } catch (Exception e1) {
                                Toast.makeText(TimeService.this, "exception", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } catch (Exception e) {
            Log.i("chmelar", e.toString());
        }
    }

    private boolean isInForeground() {
        return MessagesActivity.mIsInForegroundMode;
    }

    private void notification(String message) {
        if (BuildConfig.VERSION_CODE >= 17) {
            if (!isInForeground()) {
                android.support.v7.app.NotificationCompat.Builder builder =
                        (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentTitle("Doresio - Nová správa")
                                .setContentText(message);

                Intent notificationIntent = new Intent(this, MessagesActivity.class);
                PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);

                NotificationManager manager =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                manager.notify(001, builder.build());
            }
        } else if (BuildConfig.VERSION_CODE < 17) {
            android.support.v4.app.NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("Doresio.sk")
                            .setContentText(message);

            Intent notificationIntent = new Intent(this, MessagesActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(contentIntent);
            // Add as notification
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (!isInForeground()) {
                manager.notify(001, builder.build());
            }
        }

        if (!isInForeground()) {
            makeSound();
            makeVibration();
        }
    }

    private void makeVibration() {
        final Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(500);
    }

    private void makeSound() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
    }
}