package com.chmelar.doresiosk;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by jozef.chmelar.ml on 21. 4. 2016.
 */
@SuppressWarnings("serial")
public class Message implements Serializable {

    private String count;
    private String author;

    public int getCount() {
        return Integer.parseInt(count);
    }

    public void setCount(int count) {
        this.count = count + "";
    }

    public void setCount(String count) {
        this.count = count + "";
    }

    public String getAuthor() {
        return author;
    }

    public Message(String count, String author) {
        this.count = count;
        this.author = author;
    }

    @Override
    public String toString() {
        return "Máte " + this.count + " neprečítaných správ od " + this.author;
    }

    public boolean isSameAuthor(Message m) {
        return m.getAuthor().equals(this.getAuthor());
    }

    @Override
    public boolean equals(Object o) {
        Message m = (Message) o;
        boolean message = m.getCount() == this.getCount();
        boolean author = m.getAuthor().equals(this.getAuthor());
        return message && author;
    }
}