package com.chmelar.doresiosk;

/**
 * Created by Jozef on 23.4.2016.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartupIntentReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, TimeService.class);
        context.startService(serviceIntent);
    }
}