package com.chmelar.doresiosk;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MessagesActivity extends AppCompatActivity {

    @Bind(R.id.textViewResult)
    TextView spravy;
    @Bind(R.id.textViewStatus)
    TextView status;
    private Boolean isNewMessage;

    private ArrayList<Message> listMessages = new ArrayList<>();
    public static boolean mIsInForegroundMode = false;

    public static boolean stopNotificaton = false;
    private boolean added = false;


    //  private Context rcvContext = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        ButterKnife.bind(this);
        mIsInForegroundMode = true;
        //zacne background service
        startService(new Intent(this, TimeService.class));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("my-event"));
        TimeService.isLogged=true;
        //vypyta si spravy
        update();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsInForegroundMode = true;
        stopNotificaton = true;
        clearNotification();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("my-event"));
        update();
    }


    @Override
    protected void onPause() {
        super.onPause();
        stopNotificaton = false;
        mIsInForegroundMode = false;
    }

    @OnClick(R.id.buttonLogout)
    void btnLogout() {
        SharedPreferences prefs = this.getSharedPreferences(
                "com.chmelar.doresiosk", Context.MODE_PRIVATE);
        prefs.edit().clear().commit();
        // na stranke klikne na logout
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        // vyziada aby vymazalo data na login obrazovke
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("flag", "no");
        //koniec service
        Ion.getDefault(this).cancelAll();
        stopService(new Intent(this, TimeService.class));

        startActivity(intent);
        finish();
    }

    @OnClick(R.id.textViewResult)
    void update() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm | dd.MM.yyyy");
        String currentDateandTime = sdf.format(new Date());
        try {
            if (listMessages.isEmpty() && listMessages!=null ) {
                spravy.setText("Nemáte žiadne nové správy");
                clearNotification();
            } else if(listMessages==null){
                spravy.setText("loading");
            }
            else {
                spravy.setText("");
                for (Message message : listMessages) {
                    spravy.append(message + "\n");
                }
                spravy.append("\nupdate : " + currentDateandTime + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    // handler for received Intents for the "my-event" event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("receiver", "Got message: " + message);
            listMessages = (ArrayList<Message>) intent.getSerializableExtra("list");
            update();
        }
    };

    private void clearNotification() {
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancelAll();
    }

    public boolean isInForeground() {
        return mIsInForegroundMode;
    }


}


